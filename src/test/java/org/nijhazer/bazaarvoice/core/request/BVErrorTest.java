package org.nijhazer.bazaarvoice.core.request;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class BVErrorTest {
    private BVError error;

    private String code = "SAMPLE_CODE CODE";
    private String message = "This is a delightful sample message";

    @Before
    public void setUp() throws Exception {
        error = new BVError();
    }

    @Test
    public void testNonDefaultConstructorForCode() throws Exception {
        error = new BVError(code, message);
        Assert.assertThat(error.getCode(), is(code));
    }

    @Test
    public void testNonDefaultConstructorForMessage() throws Exception {
        error = new BVError(code, message);
        Assert.assertThat(error.getMessage(), is(message));
    }

    @Test
    public void testGetAndSetCode() throws Exception {
        error.setCode(code);
        Assert.assertThat(error.getCode(), is(code));
    }

    @Test
    public void testGetMessage() throws Exception {
        error.setMessage(message);
        Assert.assertThat(error.getMessage(), is(message));
    }
}
