package org.nijhazer.bazaarvoice.core.request;

import org.nijhazer.bazaarvoice.core.model.BVElement;
import org.nijhazer.bazaarvoice.core.model.statistic.BVReviewStatistic;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;

public class BVResponseTest {
    private BVResponse response;

    private JSONObject includes;
    private List<BVError> errors;
    private long limit = 123456789;
    private long offset = 234567890;
    private long totalResults = 345678901;
    private String locale = "en_US";
    private List<BVElement> results;
    private BVReviewStatistic sampleStatistics;

    @Before
    public void setUp() throws Exception {
        response = new BVResponse();

        includes = new JSONObject();
        includes.put("what_is_this", "kool aid man");
        includes.put("why_do_i_keep_adding_things_to_this", "no idea");

        errors = new ArrayList<BVError>();
        errors.add(new BVError("stage1", "the gracial fortress"));
        errors.add(new BVError("stage2", "does it actually matter"));

        sampleStatistics = new BVReviewStatistic();
        sampleStatistics.setAverageOverallRating(12345.678);
        sampleStatistics.setOverallRatingRange(12345);
        sampleStatistics.setTotalReviewCount(23456);
        results = new ArrayList<BVElement>();
        results.add(sampleStatistics);
    }

    @Test
    public void testGetAndSetIncludes() throws Exception {
        response.setIncludes(includes);
        Assert.assertThat(response.getIncludes(), is(includes));
    }

    @Test
    public void testHasErrors() throws Exception {
        response.setHasErrors(true);
        Assert.assertTrue(response.hasErrors());
    }

    @Test
    public void testGetAndSetLimit() throws Exception {
        response.setLimit(limit);
        Assert.assertThat(response.getLimit(), is(limit));
    }

    @Test
    public void testGetAndSetOffset() throws Exception {
        response.setOffset(offset);
        Assert.assertThat(response.getOffset(), is(offset));
    }

    @Test
    public void testGetAndSetTotalResults() throws Exception {
        response.setTotalResults(totalResults);
        Assert.assertThat(response.getTotalResults(), is(totalResults));
    }

    @Test
    public void testGetAndSetLocale() throws Exception {
        response.setLocale(locale);
        Assert.assertThat(response.getLocale(), is(locale));
    }

    @Test
    public void testGetAndSetErrors() throws Exception {
        response.setErrors(errors);
        Assert.assertThat(response.getErrors(), is(errors));
    }

    @Test
    public void testGetAndSetResults() throws Exception {
        response.setResults(results);
        Assert.assertThat(response.getResults(), is(results));
    }
}
