package org.nijhazer.bazaarvoice.core.model.statistic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class BVProductStatisticTest {
    private BVProductStatistic statistic;
    private BVReviewStatistic reviewStatistics;
    private BVReviewStatistic nativeReviewStatistics;

    String sampleProductId = "PRODUCT12345";

    @Before
    public void setUp() throws Exception {
        statistic = new BVProductStatistic();

        reviewStatistics = new BVReviewStatistic();
        reviewStatistics.setAverageOverallRating(12345.6789);
        reviewStatistics.setOverallRatingRange(555666);
        reviewStatistics.setTotalReviewCount(23456);

        nativeReviewStatistics = new BVReviewStatistic();
        nativeReviewStatistics.setAverageOverallRating(456778.890);
        nativeReviewStatistics.setOverallRatingRange(456757);
        nativeReviewStatistics.setTotalReviewCount(435456);
    }

    @Test
    public void testGetAndSetProductId() throws Exception {
        statistic.setProductId(sampleProductId);
        Assert.assertThat(statistic.getProductId(), is(sampleProductId));
    }

    @Test
    public void testGetAndSetReviewStatistics() throws Exception {
        statistic.setReviewStatistics(reviewStatistics);
        Assert.assertThat(statistic.getReviewStatistics(), is(reviewStatistics));
    }

    @Test
    public void testGetAndSetNativeReviewStatistics() throws Exception {
        statistic.setNativeReviewStatistics(nativeReviewStatistics);
        Assert.assertThat(statistic.getNativeReviewStatistics(), is(nativeReviewStatistics));
    }
}
