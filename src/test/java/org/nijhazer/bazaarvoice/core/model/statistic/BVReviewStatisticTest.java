package org.nijhazer.bazaarvoice.core.model.statistic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class BVReviewStatisticTest {
    private BVReviewStatistic statistic;

    private double averageOverallRating = 12345.67890;
    private long totalReviewCount = 958687;
    private long overallRatingRange = 3245464;

    @Before
    public void setUp() throws Exception {
        statistic = new BVReviewStatistic();
    }

    @Test
    public void testGetAndSetTotalReviewCount() throws Exception {
        statistic.setTotalReviewCount(totalReviewCount);
        Assert.assertThat(statistic.getTotalReviewCount(), is(totalReviewCount));
    }

    @Test
    public void testGetAndSetOverallRatingRange() throws Exception {
        statistic.setOverallRatingRange(overallRatingRange);
        Assert.assertThat(statistic.getOverallRatingRange(), is(overallRatingRange));
    }

    @Test
    public void testGetAndSetAverageOverallRating() throws Exception {
        statistic.setAverageOverallRating(averageOverallRating);
        Assert.assertThat(statistic.getAverageOverallRating(), is(averageOverallRating));
    }
}
