package org.nijhazer.bazaarvoice.core;

import org.junit.Assert;
import org.junit.Test;

public class BVConstantsTest {
    @Test
    public void shouldReturnExpectedValue() throws Exception {
        Assert.assertEquals(BVConstants.HTTP.toString(), "http://");
    }
}
