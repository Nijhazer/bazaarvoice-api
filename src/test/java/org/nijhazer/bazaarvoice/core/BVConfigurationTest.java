/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.InputStream;

@RunWith(MockitoJUnitRunner.class)
public class BVConfigurationTest {
    private String testProperty = "bv.api.version";
    private String defaultConfigurationFile = "bv.properties";
    private String testDefaultValue = "5.3";
    private String alternativeConfigurationFile = "test_bv_config.properties";
    private String testAlternativeValue = "salamander";
    private String nonexistantConfigurationFile = "filethatdoesntexist.properties";
    private String nonexistantProperty = "nonexistant.property";
    private BVConfiguration configuration;

    @Mock
    private InputStream mockInputStream;

    class BVConfigTestSampleObject {
        private String value;

        BVConfigTestSampleObject(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }

    @Before
    public void setUp() throws Exception {
        configuration = new BVConfiguration();
        configuration.loadFromClasspath(defaultConfigurationFile);
    }

    @Test
    public void shouldReportDefaultFilename() throws Exception {
        Assert.assertEquals(defaultConfigurationFile, configuration.getSourceFilename());
    }

    @Test
    public void shouldReportAlternativeFilenameWhereApplicable() throws Exception {
        configuration = new BVConfiguration(alternativeConfigurationFile, true);
        Assert.assertEquals(alternativeConfigurationFile, configuration.getSourceFilename());
    }

    @Test
    public void getMethodShouldReturnExpectedValue() throws Exception {
        Assert.assertEquals(testDefaultValue, configuration.get(testProperty));
    }

    @Test
    public void getMethodShouldReturnExpectedAlternativeValue() throws Exception {
        configuration = new BVConfiguration(alternativeConfigurationFile, true);
        Assert.assertEquals(testAlternativeValue, configuration.get(testProperty));
    }

    @Test
    public void getMethodShouldCallToStringToGetValueForObjects() throws Exception {
        Assert.assertEquals(testDefaultValue, configuration.get(new BVConfigTestSampleObject(testProperty)));
    }

    @Test
    public void shouldReturnNullForNonexistantProperties() {
        Assert.assertNull(configuration.get(nonexistantProperty));
    }

    @Test
    public void shouldReturnNullForExistantPropertyOnNonexistantFile() {
        configuration = new BVConfiguration(nonexistantConfigurationFile, true);
        Assert.assertNull(configuration.get(testProperty));
    }

    @Test
    public void propertiesShouldBeUnavailableInEventOfIOExceptionWhenLoading() throws Exception {
        /**
         * The BVConfiguration class uses an internal Properties object to store configuration elements.
         * In order to trigger an IOException, we must use a mock object with the same buffer size
         * that the Properties object uses. As of JDK 1.6_37, this value is 8192.
         */
        int bufferSize = 8192;
        doThrow(IOException.class).when(mockInputStream).read(new byte[bufferSize]);

        configuration = new BVConfiguration();
        configuration.load(mockInputStream);

        Assert.assertNull(configuration.get(testProperty));
    }
}
