/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.model.statistic;

import org.nijhazer.bazaarvoice.core.model.BVElement;

public class BVProductStatistic implements BVElement {
    private String productId;
    private BVReviewStatistic reviewStatistics;
    private BVReviewStatistic nativeReviewStatistics;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BVReviewStatistic getReviewStatistics() {
        return reviewStatistics;
    }

    public void setReviewStatistics(BVReviewStatistic reviewStatistics) {
        this.reviewStatistics = reviewStatistics;
    }

    public BVReviewStatistic getNativeReviewStatistics() {
        return nativeReviewStatistics;
    }

    public void setNativeReviewStatistics(BVReviewStatistic nativeReviewStatistics) {
        this.nativeReviewStatistics = nativeReviewStatistics;
    }
}
