/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.model.statistic;

import org.nijhazer.bazaarvoice.core.model.BVElement;

public class BVReviewStatistic implements BVElement {
    private long totalReviewCount;
    private long overallRatingRange;
    private double averageOverallRating;

    public long getTotalReviewCount() {
        return totalReviewCount;
    }

    public void setTotalReviewCount(long totalReviewCount) {
        this.totalReviewCount = totalReviewCount;
    }

    public long getOverallRatingRange() {
        return overallRatingRange;
    }

    public void setOverallRatingRange(long overallRatingRange) {
        this.overallRatingRange = overallRatingRange;
    }

    public double getAverageOverallRating() {
        return averageOverallRating;
    }

    public void setAverageOverallRating(double averageOverallRating) {
        this.averageOverallRating = averageOverallRating;
    }
}
