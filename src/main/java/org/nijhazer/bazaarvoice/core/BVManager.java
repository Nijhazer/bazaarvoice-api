/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core;

import org.nijhazer.bazaarvoice.core.model.BVElement;
import org.nijhazer.bazaarvoice.core.model.statistic.BVProductStatistic;
import org.nijhazer.bazaarvoice.core.processor.BVRequestJSONProcessor;
import org.nijhazer.bazaarvoice.core.processor.BVReviewStatisticsJSONProcessor;
import org.nijhazer.bazaarvoice.core.request.BVRequest;
import org.nijhazer.bazaarvoice.core.request.BVResponse;

import java.util.*;

public class BVManager {
    private BVConfiguration configuration;
    private BVResponse mostRecentAPIResponse;
    private long mostRecentAPICallProcessingTime;
    private String locale;
    private int batchSize = 20;

    public List<BVProductStatistic> getProductStatistics(String... productIds) {
        List<String> productIdsList = new ArrayList<String>();
        Collections.addAll(productIdsList, productIds);
        return getProductStatisticsBatch(productIdsList, batchSize);
    }

    public List<BVProductStatistic> getProductStatisticsBatch(List<String> productIds) {
        return getProductStatisticsBatch(productIds, batchSize);
    }

    public List<BVProductStatistic> getProductStatisticsBatch(List<String> productIds, int batchSize) {
        List<BVProductStatistic> results = new ArrayList<BVProductStatistic>();

        List<List<String>> batches = batchStrings(productIds, batchSize);
        for (List<String> batch : batches) {
            results.addAll(getProductStatistics(batch));
        }

        return results;
    }

    public List<BVProductStatistic> getProductStatistics(List<String> productIds) {
        List<BVProductStatistic> results = new ArrayList<BVProductStatistic>();

        BVRequest request = buildRequest();
        request.setResource(configuration.get(BVConstants.RESOURCE_STATISTICS));
        StringBuilder productIdsBuilder = new StringBuilder();
        boolean first = true;
        for (String productId : productIds) {
            if (first) {
                first = false;
            } else {
                productIdsBuilder.append(",");
            }
            productIdsBuilder.append(productId);
        }
        request.addParameter(BVConstants.URL_FILTER, String.format("productid:%s", productIdsBuilder.toString()));
        request.addParameter(BVConstants.URL_STATS, "Reviews,NativeReviews");
        request.addParameter(BVConstants.URL_LIMIT, "99");
        if (getLocale() != null) {
            request.addParameter(BVConstants.URL_FILTER, String.format("%s:%s:%s", BVConstants.URL_LOCALE, BVConstants.URL_EQUALS, getLocale()));
        }
        request.addRequestProcessor(new BVReviewStatisticsJSONProcessor());

        long startTime = Calendar.getInstance().getTimeInMillis();
        BVResponse response = request.process();
        this.setMostRecentAPIResponse(response);
        this.setMostRecentAPICallProcessingTime(Calendar.getInstance().getTimeInMillis() - startTime);

        for (BVElement result : response.getResults()) {
            results.add((BVProductStatistic) result);
        }
        return results;
    }

    private static List<List<String>> batchStrings(List<String> strings, int batchSize) {
        List<List<String>> batches = new ArrayList<List<String>>();
        int numberOfStrings = strings.size();

        List<String> batch = new ArrayList<String>();
        for (int i = 0; i < numberOfStrings; i++) {
            if ((i > 0) && (i % batchSize == 0)) {
                batches.add(batch);
                batch = new ArrayList<String>();
            }
            batch.add(strings.get(i));
        }
        batches.add(batch);
        return batches;
    }

    private BVRequest buildRequest() {
        BVRequest request = new BVRequest();
        request.setBaseURL(configuration.get(BVConstants.RESOURCE_BASE));
        request.setApiVersion(configuration.get(BVConstants.CONFIG_API_VERSION));
        if (configuration.get(BVConstants.CONFIG_STAGING).equalsIgnoreCase("true")) {
            request.setStaging(true);
            request.setApiKey(configuration.get(BVConstants.CONFIG_API_KEY_STAGING));
        } else {
            request.setStaging(false);
            request.setApiKey(configuration.get(BVConstants.CONFIG_API_KEY_PRODUCTION));
        }
        request.addRequestProcessor(new BVRequestJSONProcessor());
        return request;
    }

    public BVResponse getMostRecentAPIResponse() {
        return mostRecentAPIResponse;
    }

    private void setMostRecentAPIResponse(BVResponse mostRecentAPIResponse) {
        this.mostRecentAPIResponse = mostRecentAPIResponse;
    }

    public long getMostRecentAPICallProcessingTime() {
        return mostRecentAPICallProcessingTime;
    }

    private void setMostRecentAPICallProcessingTime(long mostRecentAPICallProcessingTime) {
        this.mostRecentAPICallProcessingTime = mostRecentAPICallProcessingTime;
    }

    public void setConfiguration(BVConfiguration configuration) {
        this.configuration = configuration;
    }

    public BVConfiguration getConfiguration() {
        return configuration;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
