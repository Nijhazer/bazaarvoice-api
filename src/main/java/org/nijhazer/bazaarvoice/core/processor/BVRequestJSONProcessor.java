/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.processor;

import org.nijhazer.bazaarvoice.core.BVConstants;
import org.nijhazer.bazaarvoice.core.request.BVError;
import org.nijhazer.bazaarvoice.core.request.BVResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BVRequestJSONProcessor extends BVAbstractJSONProcessor {

    public BVResponse update(JSONObject json, BVResponse elementToUpdate) {
        try {
            elementToUpdate.setHasErrors(json.getBoolean(BVConstants.JSON_HAS_ERRORS.toString()));
        } catch (JSONException e) {
            logJSONException(BVConstants.JSON_HAS_ERRORS.toString(), e);
        }
        try {
            elementToUpdate.setIncludes(json.getJSONObject(BVConstants.JSON_INCLUDES.toString()));
        } catch (JSONException e) {
            logJSONException(BVConstants.JSON_INCLUDES.toString(), e);
        }
        try {
            List<BVError> responseErrors = new ArrayList<BVError>();
            JSONArray errors = json.getJSONArray(BVConstants.JSON_ERRORS.toString());
            int numberOfErrors = errors.length();
            for (int i = 0; i < numberOfErrors; i++) {
                JSONObject error = errors.getJSONObject(i);
                BVError responseError = new BVError();
                responseError.setCode(error.getString(BVConstants.JSON_CODE.toString()));
                responseError.setMessage(error.getString(BVConstants.JSON_MESSAGE.toString()));
                responseErrors.add(responseError);
            }
            elementToUpdate.setErrors(responseErrors);
        } catch (JSONException e) {
            logJSONException(BVConstants.JSON_ERRORS.toString(), e);
        }
        try {
            elementToUpdate.setOffset(json.getLong(BVConstants.JSON_OFFSET.toString()));
        } catch (JSONException e) {
            logJSONException(BVConstants.JSON_OFFSET.toString(), e);
        }
        try {
            elementToUpdate.setLocale(json.getString(BVConstants.JSON_LOCALE.toString()));
        } catch (JSONException e) {
            logJSONException(BVConstants.JSON_LOCALE.toString(), e);
        }
        try {
            elementToUpdate.setTotalResults(json.getLong(BVConstants.JSON_TOTAL_RESULTS.toString()));
        } catch (JSONException e) {
            logJSONException(BVConstants.JSON_TOTAL_RESULTS.toString(), e);
        }
        return elementToUpdate;
    }
}
