/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.processor;

import org.nijhazer.bazaarvoice.core.BVConstants;
import org.nijhazer.bazaarvoice.core.request.BVResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BVAbstractJSONProcessor implements BVJSONProcessor {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String EXCEPTION_PATTERN = "{}{}:\n{}";

    public abstract BVResponse update(JSONObject json, BVResponse elementToUpdate);

    protected void logJSONException(String property, JSONException e) {
        logger.error(EXCEPTION_PATTERN, BVConstants.JSON_EXCEPTION_HEADER, property, e);
    }

    protected JSONArray getResults(JSONObject json) {
        JSONArray results = null;
        try {
            results = json.getJSONArray(BVConstants.JSON_RESULTS.toString());
        } catch (JSONException e) {
            logJSONException(BVConstants.JSON_RESULTS.toString(), e);
        }
        return results;
    }
}
