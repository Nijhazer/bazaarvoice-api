/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.processor;

import org.nijhazer.bazaarvoice.core.*;
import org.nijhazer.bazaarvoice.core.model.BVElement;
import org.nijhazer.bazaarvoice.core.model.statistic.BVProductStatistic;
import org.nijhazer.bazaarvoice.core.model.statistic.BVReviewStatistic;
import org.nijhazer.bazaarvoice.core.request.BVResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BVReviewStatisticsJSONProcessor extends BVAbstractJSONProcessor {
    private static String ITERATIVE_EXCEPTION_PATTERN = "%s-%s";

    public BVResponse update(JSONObject json, BVResponse elementToUpdate) {
        List<BVElement> productStatisticsList = new ArrayList<BVElement>();
        JSONArray results = this.getResults(json);
        int numberOfEntries = results.length();
        for (int i = 0; i < numberOfEntries; i++) {
            BVProductStatistic responseProductStatistics = new BVProductStatistic();
            JSONObject productStatistics = null;
            try {
                productStatistics = results.getJSONObject(i).getJSONObject(BVConstants.JSON_PRODUCT_STATISTICS.toString());
            } catch (JSONException e) {
                logJSONException(String.format(ITERATIVE_EXCEPTION_PATTERN, BVConstants.JSON_PRODUCT_STATISTICS, i), e);
            }
            try {
                responseProductStatistics.setProductId(productStatistics.getString(BVConstants.JSON_PRODUCT_ID.toString()));
            } catch (JSONException e) {
                logJSONException(String.format(ITERATIVE_EXCEPTION_PATTERN, BVConstants.JSON_PRODUCT_ID, i), e);
            }
            try {
                responseProductStatistics.setReviewStatistics(
                    processReviewStatistics(
                        productStatistics.getJSONObject(
                            BVConstants.JSON_REVIEW_STATISTICS.toString())));
            } catch (JSONException e) {
                logJSONException(String.format(ITERATIVE_EXCEPTION_PATTERN, BVConstants.JSON_REVIEW_STATISTICS, i), e);
            } catch (BVJSONProcessingException e) {
                logJSONException(String.format(ITERATIVE_EXCEPTION_PATTERN, e.getValueInError(), i), e.getOriginalJSONException());
            }
            try {
                responseProductStatistics.setNativeReviewStatistics(
                    processReviewStatistics(
                        productStatistics.getJSONObject(
                            BVConstants.JSON_NATIVE_REVIEW_STATISTICS.toString())));
            } catch (JSONException e) {
                logJSONException(String.format(ITERATIVE_EXCEPTION_PATTERN, BVConstants.JSON_NATIVE_REVIEW_STATISTICS, i), e);
            } catch (BVJSONProcessingException e) {
                logJSONException(String.format(ITERATIVE_EXCEPTION_PATTERN, e.getValueInError(), i), e.getOriginalJSONException());
            }
            productStatisticsList.add(responseProductStatistics);
        }
        elementToUpdate.setResults(productStatisticsList);
        return elementToUpdate;
    }

    private BVReviewStatistic processReviewStatistics(JSONObject source) throws BVJSONProcessingException {
        BVReviewStatistic reviewStatistics = new BVReviewStatistic();
        if (source != null) {
            try {
                reviewStatistics.setTotalReviewCount(source.getLong(BVConstants.JSON_TOTAL_REVIEW_COUNT.toString()));
            } catch (JSONException e) {
                throw new BVJSONProcessingException(BVConstants.JSON_TOTAL_REVIEW_COUNT.toString(), e);
            }
            try {
                reviewStatistics.setOverallRatingRange(source.getLong(BVConstants.JSON_OVERALL_RATING_RANGE.toString()));
            } catch (JSONException e) {
                throw new BVJSONProcessingException(BVConstants.JSON_OVERALL_RATING_RANGE.toString(), e);
            }
            try {
                reviewStatistics.setAverageOverallRating(source.getDouble(BVConstants.JSON_AVERAGE_OVERALL_RATING.toString()));
            } catch (JSONException e) {
                throw new BVJSONProcessingException(BVConstants.JSON_AVERAGE_OVERALL_RATING.toString(), e);
            }
        }
        return reviewStatistics;
    }
}
