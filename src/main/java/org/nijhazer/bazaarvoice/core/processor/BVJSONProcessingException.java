/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.processor;

import org.json.JSONException;

public class BVJSONProcessingException extends Exception {
    private String valueInError;
    private JSONException originalJSONException;

    public BVJSONProcessingException(String valueInError) {
        super();
        setValueInError(valueInError);
    }

    public BVJSONProcessingException(String valueInError, JSONException originalJSONException) {
        this(valueInError);
        setOriginalJSONException(originalJSONException);
    }

    public final String getValueInError() {
        return valueInError;
    }

    public final void setValueInError(String valueInError) {
        this.valueInError = valueInError;
    }

    public final JSONException getOriginalJSONException() {
        return originalJSONException;
    }

    public final void setOriginalJSONException(JSONException originalJSONException) {
        this.originalJSONException = originalJSONException;
    }
}
