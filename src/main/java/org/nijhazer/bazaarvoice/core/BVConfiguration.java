/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

public class BVConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(BVConfiguration.class);
    private Properties properties;
    private String sourceFilename;

    public BVConfiguration() {
        properties = new Properties();
    }

    public BVConfiguration(String filename) {
        this(filename, false);
    }

    public BVConfiguration(String filename, boolean loadFromClasspath) {
        this();
        if (loadFromClasspath) {
            loadFromClasspath(filename);
        } else {
            loadFromFile(filename);
        }
    }

    public void loadFromClasspath(String filename) {
        setSourceFilename(filename);
        filename = String.format("/%s", filename);
        InputStream inputStream = ClassLoader.class.getResourceAsStream(filename);
        if (inputStream == null) {
            logger.error("Properties file '{}' was not found on classpath.", filename);
            return;
        }
        load(inputStream);
        try {
            inputStream.close();
        } catch (IOException e) {
            logger.error("Unable to close classpath file '{}'", filename, e);
        }
    }

    public void loadFromFile(String filename) {
        setSourceFilename(filename);
        InputStream inputStream = null;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(filename));
        } catch (FileNotFoundException e) {
            logger.error("Properties file '{}' was not found.", filename, e);
            return;
        }
        load(inputStream);
        try {
            inputStream.close();
        } catch (IOException e) {
            logger.error("Unable to close file '{}'", filename, e);
        }
    }

    public void load(InputStream inputStream) {
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.error("Unable to read properties from input source '{}'", inputStream, e);
        }
    }

    public String getSourceFilename() {
        return sourceFilename;
    }

    private void setSourceFilename(String sourceFilename) {
        this.sourceFilename = sourceFilename;
    }

    public String get(Object key) {
        return get(key.toString());
    }

    public String get(String key) {
        return properties.get(key) == null ? null : (String) properties.get(key);
    }
}
