/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.request;

import org.nijhazer.bazaarvoice.core.model.BVElement;
import org.json.JSONObject;

import java.util.List;

public class BVResponse implements BVElement {
    private JSONObject includes;
    private long limit;
    private long offset;
    private long totalResults;
    private String locale;
    private boolean hasErrors;
    private List<BVError> errors;
    private List<BVElement> results;

    public JSONObject getIncludes() {
        return includes;
    }

    public void setIncludes(JSONObject includes) {
        this.includes = includes;
    }

    public boolean hasErrors() {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public long getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(long totalResults) {
        this.totalResults = totalResults;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public List<BVError> getErrors() {
        return errors;
    }

    public void setErrors(List<BVError> errors) {
        this.errors = errors;
    }

    public List<BVElement> getResults() {
        return results;
    }

    public void setResults(List<BVElement> results) {
        this.results = results;
    }
}
