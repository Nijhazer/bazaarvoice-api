/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core.request;

import org.nijhazer.bazaarvoice.core.BVConstants;
import org.nijhazer.bazaarvoice.core.model.BVElement;
import org.nijhazer.bazaarvoice.core.processor.BVJSONProcessor;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BVRequest implements BVElement {
    private static final Logger logger = LoggerFactory.getLogger(BVRequest.class);

    private String baseURL;
    private String resource;
    private String apiVersion;
    private String apiKey;
    private boolean staging;
    private Map<String, List<String>> requestParameters;
    private List<BVJSONProcessor> requestProcessors;

    public BVRequest() {
        this.requestParameters = new HashMap<String, List<String>>();
        this.requestProcessors = new ArrayList<BVJSONProcessor>();
    }

    public final BVResponse process() {
        BVResponse response = new BVResponse();
        String responseData = getRawAPIResponseData();

        JSONObject json = null;
        try {
            json = new JSONObject(responseData);
            logger.trace("Response JSON: {}", json);
        } catch (JSONException e) {
            logger.error("JSONException when processing response data", e);
            return null;
        }

        for (BVJSONProcessor requestProcessor : this.requestProcessors) {
            response = requestProcessor.update(json, response);
        }

        return response;
    }

    public final URL buildURL() {
        return buildURL(false);
    }

    public final URL buildURL(boolean secure) {
        URL url = null;
        StringBuilder urlBuilder = new StringBuilder();
        if (secure) {
            urlBuilder.append(BVConstants.HTTPS);
        } else {
            urlBuilder.append(BVConstants.HTTP);
        }
        urlBuilder
            .append(this.getBaseURL())
            .append("/");
        if (isStaging()) {
            urlBuilder
            .append(BVConstants.URL_STAGING)
            .append("/");
        }
        urlBuilder
            .append(BVConstants.URL_DATA)
            .append("/")
            .append(this.getResource())
            .append("?")
            .append(BVConstants.URL_API_VERSION)
            .append("=")
            .append(this.getApiVersion())
            .append("&")
            .append(BVConstants.URL_API_KEY)
            .append("=")
            .append(this.getApiKey());
        for (String parameterKey : requestParameters.keySet()) {
            for (String parameterValue : requestParameters.get(parameterKey)) {
                urlBuilder
                    .append("&")
                    .append(parameterKey)
                    .append("=")
                    .append(parameterValue);
            }
        }
        logger.debug("URL for BV request: {}", urlBuilder);
        try {
            url = new URL(urlBuilder.toString());
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException when building URL", e);
        }
        return url;
    }

    public final InputStream getInputStream(URL url) {
        InputStream inputStream = null;
        try {
            inputStream = url.openStream();
        } catch (IOException e) {
            logger.error("IOException when opening input stream from URL {}", url, e);
        }
        return inputStream;
    }

    public final String getRawAPIResponseData() {
        String responseData = null;
        InputStream inputStream = getInputStream(buildURL());
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(inputStream));
        try {
            responseData = reader.readLine();
        } catch (IOException e) {
            logger.error("Exception when retrieving response data from API", e);
        }
        return responseData;
    }

    public final void addParameter(Object parameter, String value) {
        List<String> existingValues = requestParameters.get(parameter.toString());
        if (existingValues == null) {
            existingValues = new ArrayList<String>();
        }
        existingValues.add(value);
        requestParameters.put(parameter.toString(), existingValues);
    }

    public final void addRequestProcessor(BVJSONProcessor processor) {
        this.requestProcessors.add(processor);
    }

    public final String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    public final String getResource() {
        return resource;
    }

    public final void setResource(String resource) {
        this.resource = resource;
    }

    public final String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public final String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public final boolean isStaging() {
        return staging;
    }

    public final void setStaging(boolean staging) {
        this.staging = staging;
    }
}