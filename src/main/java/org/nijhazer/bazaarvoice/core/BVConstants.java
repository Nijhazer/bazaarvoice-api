/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core;

public enum BVConstants {
    HTTP("http://"),
    HTTPS("https://"),
    RESOURCE_BASE("bv.resource.base"),
    RESOURCE_STATISTICS("bv.resource.statistics"),
    CONFIG_API_VERSION("bv.api.version"),
    CONFIG_API_KEY_PRODUCTION("bv.api.key.production"),
    CONFIG_API_KEY_STAGING("bv.api.key.staging"),
    CONFIG_STAGING("bv.staging"),
    URL_STAGING("bvstaging"),
    URL_DATA("data"),
    URL_API_VERSION("apiversion"),
    URL_API_KEY("passkey"),
    URL_FILTER("filter"),
    URL_STATS("stats"),
    URL_LIMIT("limit"),
    URL_LOCALE("contentlocale"),
    URL_EQUALS("eq"),
    JSON_EXCEPTION_HEADER("JSONException encountered while parsing value: "),
    JSON_HAS_ERRORS("HasErrors"),
    JSON_INCLUDES("Includes"),
    JSON_ERRORS("Errors"),
    JSON_CODE("Code"),
    JSON_MESSAGE("Message"),
    JSON_OFFSET("Offset"),
    JSON_LOCALE("Locale"),
    JSON_TOTAL_RESULTS("TotalResults"),
    JSON_RESULTS("Results"),
    JSON_PRODUCT_STATISTICS("ProductStatistics"),
    JSON_NATIVE_REVIEW_STATISTICS("NativeReviewStatistics"),
    JSON_TOTAL_REVIEW_COUNT("TotalReviewCount"),
    JSON_AVERAGE_OVERALL_RATING("AverageOverallRating"),
    JSON_OVERALL_RATING_RANGE("OverallRatingRange"),
    JSON_REVIEW_STATISTICS("ReviewStatistics"),
    JSON_PRODUCT_ID("ProductId");

    private final String text;

    private BVConstants(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
