/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.nijhazer.bazaarvoice.core;

public class BV {
    private static BVConfiguration configuration;

    private static void configure() {
        if (configuration == null) {
            configuration = new BVConfiguration("./bv.properties");
        }
    }

    public static void loadConfigurationFromFile(String filename) {
        configuration = new BVConfiguration();
        configuration.loadFromFile(filename);
    }

    public static BVManager getBVManager() {
        configure();
        BVManager manager = new BVManager();
        manager.setConfiguration(configuration);
        return manager;
    }
}
